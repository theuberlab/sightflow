//+build mage

package main

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
	"github.com/magefile/mage/target"
	"github.com/prometheus/common/log"

	// mage:import bintray
	"gitlab.com/theuberlab/sightflow/mage/bintray"
	// mage:import version
	"gitlab.com/theuberlab/sightflow/mage/version"
)

var (
	repoBase     string
	buildDir     string
	buildDirName = "build"
	binName      = "sightflow"
)

// Enum for scheme
type OSType int

const (
	OSTYPE_LINUX OSType = iota
	OSTYPE_MACOS
	OSTYPE_WINDOWS
)

// Initializes some global variables. Do not call this target directly.
func InitVars() {
	var err error = nil
	repoBase, err = sh.Output("git", "rev-parse", "--show-toplevel")

	if err != nil {
		log.Fatal(err)
	}
	buildDir = repoBase + "/" + buildDirName
}

// Runs go fmt ./...
func Fmt() error {
	mg.Deps(InitVars)
	log.Infoln("Making Fmt")
	return sh.RunV("go", "vet", "./...")
}

// runs go vet ./...
func Vet() error {
	mg.Deps(InitVars)
	log.Infoln("Making Vet")
	return sh.RunV("go", "vet", "./...")
}

// Runs errcheck ./...
// See https://github.com/kisielk/errcheck
func ErrChk() error {
	mg.Deps(InitVars)
	log.Infoln("Making ErrChk")
	excludesFile := repoBase + "/errcheckExcludes.txt"
	log.Infof("Checking for excludes file %s\n", excludesFile)
	_, err := os.Stat(excludesFile)

	if err != nil && os.IsNotExist(err) {
		log.Info("Running errcheck without excludes.")
		return sh.RunV("errcheck", "./...")
	} else {
		log.Info("Running errcheck with excludes file.")
		return sh.RunV("errcheck", "--exclude", "errcheckExcludes.txt", "./...")
	}
}

// Runs go test ./...
func TestAll() error {
	log.Infoln("Making TestAll")
	return sh.RunV("go", "test", "./...")
}

// Depends upon Fmt, Vet, ErrChk and TestAll
func AllPreReqs() {
	log.Infoln("Making all requirements")
	mg.Deps(Fmt, Vet, ErrChk, TestAll)
}

// Creates the build directory if it doesn't exist.
func makeDirectory(dirName string) error {
	log.Debugf("Checking directory [%s]\n", dirName)
	fileInfo, err := os.Stat(dirName)

	if err != nil {
		if os.IsNotExist(err) {
			log.Infof("Creating directory [%s]\n", dirName)
			err2 := os.Mkdir(dirName, 0755)
			if err2 != nil {
				log.Infof("Error creating directory %s\n", dirName)
				return err2
			}
			return nil
		} else {
			return err
		}
	}

	switch mode := fileInfo.Mode(); {
	case mode.IsDir():
		log.Info("Directory already exists")
		return nil
	case mode.IsRegular():

		return errors.New(fmt.Sprintf("%s already exists but it is not a directory.\n", dirName))
	default:
		return errors.New(fmt.Sprintf("Unexpected file type [%v]", mode))
	}

}

// Creates the build directory.
func MakeBuildDir() error {
	mg.Deps(InitVars)
	log.Infoln("Making build directory")

	return makeDirectory(buildDir)
}

// does go list ./... and returns an array.
func getGoFilesList() []string {
	var goFiles []string
	filepath.Walk(repoBase,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if strings.HasPrefix(info.Name(), "mage") {
				// Skip if only the magefile(s) have been updated.
				return nil
			} else if strings.HasSuffix(info.Name(), ".go") {
				log.Debugf("Adding file %s to goFiles list\n", path)
				goFiles = append(goFiles, path)
			}
			return nil
		})

	goFiles = append(goFiles, repoBase+"/go.mod")
	goFiles = append(goFiles, repoBase+"/go.sum")

	return goFiles
}

// Runs go mod download and then go build ./...
func BuildForOS(osType OSType) error {
	mg.Deps(InitVars)
	mg.Deps(MakeBuildDir)
	mg.Deps(AllPreReqs)

	environ := make(map[string]string)
	environ["GOARCH"] = "amd64"
	environ["GOOS"] = "NOT SET"

	var targetName string
	switch osType {
	case OSTYPE_LINUX:
		environ["GOOS"] = "linux"
		targetName = binName + "_linux_amd64"
	case OSTYPE_MACOS:
		environ["GOOS"] = "darwin"
		targetName = binName + "_darwin_amd64"
	case OSTYPE_WINDOWS:
		environ["GOOS"] = "windows"
		targetName = binName + "_windows_amd64"
	}

	targetFile := buildDir + "/" + targetName

	goFiles := getGoFilesList()

	log.Debugf("Checking timestamps against target %s for files [%v]\n", targetFile, goFiles)
	updated, err := target.Path(targetFile, goFiles...)
	if err != nil {
		log.Fatalf("Error evaluating updates %s\n", err)
	}

	if !updated {
		log.Warnf("Nothing to do: No golang changes since last build of target file %s\n", targetFile)
		return nil
	}
	if err = sh.Run("go", "mod", "download"); err != nil {
		return err
	}

	log.Infof("Building for os %s as %s\n", environ["GOOS"], targetFile)

	curVers := version.GetCurrentVersion()

	var compRegEx = regexp.MustCompile("^v([\\d])+\\.([\\d])+\\.([\\d])+-?(.*)$")

	match := compRegEx.FindStringSubmatch(curVers)

	major, _ := strconv.Atoi(match[1])
	minor, _ := strconv.Atoi(match[2])
	patch, _ := strconv.Atoi(match[3])

	newPatch := patch + 1

	verString := fmt.Sprintf("v%d.%d.%d", major, minor, newPatch)

	if len(match) == 5 && match[4] != "" {
		verString = verString + "-" + match[4]
	}

	verFlag := fmt.Sprintf("-X gitlab.com/theuberlab/common-lang/go/version.VersAsString=%s", verString)

	log.Infof("Executing command [%s]\n", "go" + " " + "build" + " " + "-ldflags" + " '" + verFlag + "' " + "-o" + " " + targetFile + " " + repoBase + "/cmd/main.go")
	return sh.RunWithV(environ, "go", "build", "-ldflags", verFlag, "-o", targetFile, repoBase + "/cmd/main.go")
}

// Builds for linux amd64
func BuildLinux() error {
	mg.Deps(AllPreReqs)
	log.Infoln("Building for Linux")
	return BuildForOS(OSTYPE_LINUX)
}

// Builds for darwin amd64
func BuildMacOS() error {
	mg.Deps(AllPreReqs)
	log.Infoln("Building for MacOS")
	return BuildForOS(OSTYPE_MACOS)
}

// Builds for windows amd64
func BuildWindows() error {
	mg.Deps(AllPreReqs)
	log.Infoln("Building for Windows")
	return BuildForOS(OSTYPE_WINDOWS)
}

// Builds for all supported OSes.
func BuildAll() {
	mg.Deps(AllPreReqs)
	mg.Deps(BuildLinux, BuildMacOS, BuildWindows)
}

// Deletes any build artifacts.
func Clean() error {
	mg.Deps(InitVars)

	projname := filepath.Base(repoBase)

	fileInfo, err := os.Stat(repoBase + projname)

	if !os.IsNotExist(err) && fileInfo.Mode().IsRegular() && fileInfo.Mode()&0111 != 0 {
		log.Infof("Removing accidentally compiled binary %s.\n", repoBase+projname)
		err := os.Remove(repoBase + projname)
		if err != nil {
			log.Fatal(err)
		}
	}

	_, err = os.Stat(buildDir)
	if !os.IsNotExist(err) {
		log.Infof("Removing build dir and contents %s\n", buildDir)
		err = os.RemoveAll(buildDir)
		if err != nil {
			log.Fatal(err)
		}
	}

	return nil
}

// Builds for all versions then pushes to bintray.
func Publish() {
	mg.Deps(BuildAll)
	mg.Deps(bintray.ReleaseAll)
}
