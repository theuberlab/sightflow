package gitlab

import (
	"regexp"
	"time"
)

// GitlabServer represents an instance of GitLab
type GitlabServer struct {
	Scheme   string
	Host     string
	Port     int
	Token    string
	BasePath string
	IncludeRegex	*regexp.Regexp
	ExcludeRegex    *regexp.Regexp
}

// GLPipeLine is the API response from gitlab
type GLPipeLine struct {
	CreatedAt time.Time `json:"created_at"`
	ID        int       `json:"id"`
	Ref       string    `json:"ref"`
	Sha       string    `json:"sha"`
	Status    string    `json:"status"`
	UpdatedAt time.Time `json:"updated_at"`
	WebURL    string    `json:"web_url"`
}

type GLPipeLineFull struct {
	ID         int         `json:"id"`
	Sha        string      `json:"sha"`
	Ref        string      `json:"ref"`
	Status     string      `json:"status"`
	CreatedAt  time.Time   `json:"created_at"`
	UpdatedAt  time.Time   `json:"updated_at"`
	WebURL     string      `json:"web_url"`
	BeforeSha  string      `json:"before_sha"`
	Tag        bool        `json:"tag"`
	YamlErrors interface{} `json:"yaml_errors"`
	User       struct {
		ID        int    `json:"id"`
		Name      string `json:"name"`
		Username  string `json:"username"`
		State     string `json:"state"`
		AvatarURL string `json:"avatar_url"`
		WebURL    string `json:"web_url"`
	} `json:"user"`
	StartedAt      time.Time   `json:"started_at"`
	FinishedAt     time.Time   `json:"finished_at"`
	CommittedAt    interface{} `json:"committed_at"`
	Duration       int         `json:"duration"`
	Coverage       interface{} `json:"coverage"`
	DetailedStatus struct {
		Icon         string      `json:"icon"`
		Text         string      `json:"text"`
		Label        string      `json:"label"`
		Group        string      `json:"group"`
		Tooltip      string      `json:"tooltip"`
		HasDetails   bool        `json:"has_details"`
		DetailsPath  string      `json:"details_path"`
		Illustration interface{} `json:"illustration"`
		Favicon      string      `json:"favicon"`
	} `json:"detailed_status"`
}

// GLJob https://docs.gitlab.com/ee/api/jobs.html
type GLJob struct {
	AllowFailure bool `json:"allow_failure"`
	Artifacts    []struct {
		FileFormat interface{} `json:"file_format"`
		FileType   string      `json:"file_type"`
		Filename   string      `json:"filename"`
		Size       int         `json:"size"`
	} `json:"artifacts"`
	ArtifactsExpireAt interface{} `json:"artifacts_expire_at"`
	Commit            struct {
		AuthorEmail    string   `json:"author_email"`
		AuthorName     string   `json:"author_name"`
		AuthoredDate   string   `json:"authored_date"`
		CommittedDate  string   `json:"committed_date"`
		CommitterEmail string   `json:"committer_email"`
		CommitterName  string   `json:"committer_name"`
		CreatedAt      string   `json:"created_at"`
		ID             string   `json:"id"`
		Message        string   `json:"message"`
		ParentIds      []string `json:"parent_ids"`
		ShortID        string   `json:"short_id"`
		Title          string   `json:"title"`
		WebURL         string   `json:"web_url"`
	} `json:"commit"`
	Coverage   interface{} `json:"coverage"`
	CreatedAt  time.Time   `json:"created_at"`
	Duration   float64     `json:"duration"`
	FinishedAt time.Time   `json:"finished_at"`
	ID         int         `json:"id"`
	Name       string      `json:"name"`
	Pipeline   struct {
		CreatedAt time.Time `json:"created_at"`
		ID        int       `json:"id"`
		Ref       string    `json:"ref"`
		Sha       string    `json:"sha"`
		Status    string    `json:"status"`
		UpdatedAt time.Time `json:"updated_at"`
		WebURL    string    `json:"web_url"`
	} `json:"pipeline"`
	Ref    string `json:"ref"`
	Runner struct {
		Active      bool   `json:"active"`
		Description string `json:"description"`
		ID          int    `json:"id"`
		IPAddress   string `json:"ip_address"`
		IsShared    bool   `json:"is_shared"`
		Name        string `json:"name"`
		Online      bool   `json:"online"`
		Status      string `json:"status"`
	} `json:"runner"`
	Stage     string    `json:"stage"`
	StartedAt time.Time `json:"started_at"`
	Status    string    `json:"status"`
	Tag       bool      `json:"tag"`
	User      struct {
		AvatarURL       string      `json:"avatar_url"`
		Bio             string      `json:"bio"`
		BioHTML         string      `json:"bio_html"`
		CreatedAt       time.Time   `json:"created_at"`
		ID              int         `json:"id"`
		JobTitle        string      `json:"job_title"`
		Linkedin        string      `json:"linkedin"`
		Location        string      `json:"location"`
		Name            string      `json:"name"`
		Organization    string      `json:"organization"`
		PublicEmail     string      `json:"public_email"`
		Skype           string      `json:"skype"`
		State           string      `json:"state"`
		Twitter         string      `json:"twitter"`
		Username        string      `json:"username"`
		WebURL          string      `json:"web_url"`
		WebsiteURL      string      `json:"website_url"`
		WorkInformation interface{} `json:"work_information"`
	} `json:"user"`
	WebURL string `json:"web_url"`
}

// GLCommit https://docs.gitlab.com/13.6/ee/api/commits.html#get-a-single-commit
type GLCommit struct {
	ID             string    `json:"id"`
	ShortID        string    `json:"short_id"`
	Title          string    `json:"title"`
	AuthorName     string    `json:"author_name"`
	AuthorEmail    string    `json:"author_email"`
	CommitterName  string    `json:"committer_name"`
	CommitterEmail string    `json:"committer_email"`
	CreatedAt      time.Time `json:"created_at"`
	Message        string    `json:"message"`
	CommittedDate  time.Time `json:"committed_date"`
	AuthoredDate   time.Time `json:"authored_date"`
	ParentIds      []string  `json:"parent_ids"`
	LastPipeline   struct {
		ID     int    `json:"id"`
		Ref    string `json:"ref"`
		Sha    string `json:"sha"`
		Status string `json:"status"`
	} `json:"last_pipeline"`
	Stats struct {
		Additions int `json:"additions"`
		Deletions int `json:"deletions"`
		Total     int `json:"total"`
	} `json:"stats"`
	Status string `json:"status"`
	WebURL string `json:"web_url"`
}
