package gitlab

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/prometheus/common/log"
	"gitlab.com/theuberlab/sightflow/pkg/sfUtil"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
)

// GetProjectIDFromPath takes a project path like "theuberlab/sightflow" and returns the project
// id for that project.
func (g *GitlabServer) GetProjectIDFromPath(projectPath string) int {
	log.Infof("Getting ID for project: %s", projectPath)

	encodedProj := url.PathEscape(projectPath)

	var project GLPipeLineFull

	client := &http.Client{}

	url := fmt.Sprintf("%s://%s%s%s/projects/%s", g.Scheme, g.Host, fmt.Sprintf(":%d", g.Port), g.BasePath, encodedProj)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		sfUtil.Die("Error building get pipeline request %s", err)
	}

	req.Header.Add("Private-Token", g.Token)
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Connection", "close")

	response, err := client.Do(req)
	defer response.Body.Close()

	if err != nil {
		sfUtil.Die("Get pipelines Failed with error %s", err)
	}

	buf := new(bytes.Buffer)
	dec := json.NewDecoder(io.TeeReader(response.Body, buf))
	dec.UseNumber()

	if response.StatusCode != 200 {
		bodyBytes, err := ioutil.ReadAll(response.Body)
		if err != nil {
			sfUtil.Die("Error reading response body: %s\n", err)
		}
		fmt.Printf("Error fetching project ID: Response Code: %d Response: \n%s\n",response.StatusCode, string(bodyBytes))
		os.Exit(1)
	}

	err = dec.Decode(&project)
	if err != nil {
		sfUtil.Die("Error decoding json response for pipeline: %s", err)
	}

	return project.ID
}