package gitlab

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"github.com/itchyny/gojq"
	"github.com/prometheus/common/log"

	"gitlab.com/theuberlab/sightflow/pkg/sfUtil"
)

// GetLatestPipelineForProject returns a GLPipeLine of the most recent pipeline for the specified project.
func (g *GitlabServer) GetLatestPipelineForProject(projectID int) GLPipeLine {
	var result GLPipeLine

	client := &http.Client{}

	url := fmt.Sprintf("%s://%s%s%s/projects/%d/pipelines", g.Scheme, g.Host, fmt.Sprintf(":%d", g.Port), g.BasePath, projectID)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		sfUtil.Die("Error building get pipelines request %s\n", err)
	}

	req.Header.Add("Private-Token", g.Token)
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Connection", "close")

	response, err := client.Do(req)
	defer response.Body.Close()

	if err != nil {
		sfUtil.Die("Get pipelines Failed with error %s\n", err)
	}

	var responseMap []interface{}

	buf := new(bytes.Buffer)
	dec := json.NewDecoder(io.TeeReader(response.Body, buf))
	dec.UseNumber()

	err = dec.Decode(&responseMap)
	if err != nil {
		sfUtil.Die("Error decoding json response for latest pipeline: %s", err)
	}

	query, err := gojq.Parse(". |= sort_by(.created_at) | reverse")
	if err != nil {
		sfUtil.Die("Error parsing gojq query: %s\n", err)
	}

	iter := query.Run(responseMap) // or query.RunWithContext
	for {
		value, ok := iter.Next()
		if !ok {
			break
		}
		if err, ok := value.(error); ok {
			log.Errorf("Iterator has error: %s\n", err)
		}

		var theseResults []GLPipeLine

		// gojq only returns things as interface{}, []interface{} or map[string]interface{}
		// To transform marshal it to json
		//resultList, _ := json.Marshal(value.(map[string]interface{}))
		resultList, _ := json.Marshal(value.([]interface{}))
		if err, ok := value.(error); ok {
			log.Errorf("Error marshaling gojq results: %s\n", err.Error())
		}

		// Then unmarshal it back to something we understand ([]GLPipeline)
		err = json.Unmarshal(resultList, &theseResults)
		if err, ok := value.(error); ok {
			log.Errorf("Error unmarshaling gojq results: %s\n", err.Error())
		}

		for _, thisResult := range theseResults {
			// If we have an exclude regex
			if g.ExcludeRegex != nil {
				// Check if the REF matches it.
				if g.ExcludeRegex.Match([]byte(thisResult.Ref)) {
					// If it does move on to the next element without assigning result.
					continue
				}
			}
			// If we have an include regex
			if g.IncludeRegex != nil {
				// Check if the REF matches it.
				if ! g.IncludeRegex.Match([]byte(thisResult.Ref)) {
					// If it does not, move on to the next element without assigning to result.
					continue
				}
			}
			// If we haven't skipped this iteration assign the result and leave the loop
			result = thisResult
			break
		}

	}
	return result
}

// GetPipelineForProject returns GLPipeLineFull representing the specified pipeline
func (g *GitlabServer) GetPipelineForProject(projectID int, pipeline int) GLPipeLineFull {
	var result GLPipeLineFull

	client := &http.Client{}

	url := fmt.Sprintf("%s://%s%s%s/projects/%d/pipelines/%d", g.Scheme, g.Host, fmt.Sprintf(":%d", g.Port), g.BasePath, projectID, pipeline)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		sfUtil.Die("Error building get pipeline request %s\n", err)
	}

	req.Header.Add("Private-Token", g.Token)
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Connection", "close")

	response, err := client.Do(req)
	defer response.Body.Close()

	if err != nil {
		sfUtil.Die("Get pipelines Failed with error %s\n", err)
	}

	buf := new(bytes.Buffer)
	dec := json.NewDecoder(io.TeeReader(response.Body, buf))
	dec.UseNumber()

	err = dec.Decode(&result)
	if err != nil {
		sfUtil.Die("Error decoding json response for pipeline: %s\n", err)
	}

	return result
}

// GetPipelineForProject returns GLPipeLineFull representing the specified pipeline
func (g *GitlabServer) GetCommitForProject(projectID int, commitSha string) GLCommit {
	var result GLCommit

	client := &http.Client{}

	url := fmt.Sprintf("%s://%s%s%s/projects/%d/repository/commits/%s", g.Scheme, g.Host, fmt.Sprintf(":%d", g.Port), g.BasePath, projectID, commitSha)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		sfUtil.Die("Error building get commit request %s\n", err)
	}

	req.Header.Add("Private-Token", g.Token)
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Connection", "close")

	response, err := client.Do(req)
	defer response.Body.Close()

	if err != nil {
		sfUtil.Die("Get commit Failed with error %s\n", err)
	}

	buf := new(bytes.Buffer)
	dec := json.NewDecoder(io.TeeReader(response.Body, buf))
	dec.UseNumber()

	err = dec.Decode(&result)
	if err != nil {
		sfUtil.Die("Error decoding json response for commit: %s\n", err)
	}

	return result
}

// GetJobsForPipeline retrieves jobs defined for the specivied pipeline and returns them as an array of GLJob
func (g *GitlabServer) GetJobsForPipeline(projectID int, pipelineID int) []GLJob {

	client := &http.Client{}

	url := fmt.Sprintf("%s://%s%s%s/projects/%d/pipelines/%d/jobs", g.Scheme, g.Host, fmt.Sprintf(":%d", g.Port), g.BasePath, projectID, pipelineID)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		sfUtil.Die("Error building get pipelines request %s\n", err)
	}

	req.Header.Add("Private-Token", g.Token)
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Connection", "close")

	response, err := client.Do(req)
	if err != nil {
		sfUtil.Die("Unable to perform request. Error: %s\n", err)
	}

	defer func() {
		err = response.Body.Close()
		if err != nil {
			sfUtil.Die("Failed closing body with error: %s\n", err)
		}
	}()

	if response == nil {
		sfUtil.Die("No response body. Error: %s\n", err)
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		sfUtil.Die("Error reading response body: %s\n", err)
	}

	var results []GLJob
	err = json.Unmarshal(responseData, &results)
	if err != nil {
		sfUtil.Die("Error unmarshalling results: %s\n", err)
	}

	return results
}
