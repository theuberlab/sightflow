package sfUtil

import (
	"os"

	"github.com/gizak/termui/v3"
	"github.com/prometheus/common/log"
)

func Die(message string, err error) {
	termui.Render(termui.NewCanvas())
	termui.Clear()
	termui.Close()

	log.Errorf(message, err.Error())
	os.Exit(1)
}
