package ui

import (
	"fmt"
	"github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
	"github.com/prometheus/common/log"
	"gitlab.com/theuberlab/sightflow/pkg/gitlab"
)

// Display is responsible for displaying the results to the terminal.
type Display struct {
	Project    int
	UpdateChan chan bool
	NewPipeChan chan *gitlab.GLPipeLineFull
	ResizeChan chan termui.Resize
	Server      gitlab.GitlabServer
	Pipeline    *gitlab.GLPipeLineFull
	Commit      *gitlab.GLCommit
	PipeWindow  *widgets.Paragraph
	JobsWindow  *widgets.List
}

// Display displays the output
func (d *Display) Display() {
	if err := termui.Init(); err != nil {
		log.Fatalf("failed to initialize termui: %v", err)
	}

	pipelineBox := widgets.NewParagraph()
	JobsBox := widgets.NewList()

	// TODO: Do something with this anonymous function.
	updateParagraph := func(count int) {
		if count%2 == 0 {
			pipelineBox.BorderStyle.Fg = termui.ColorGreen
		} else {
			pipelineBox.BorderStyle.Fg = termui.ColorBlue
		}
	}

	grid := termui.NewGrid()
	termWidth, termHeight := termui.TerminalDimensions()
	grid.SetRect(0, 0, termWidth, termHeight)


	//grid.Border = true

	grid.Set(
		termui.NewRow(1.0*.2,
			termui.NewCol(1.0, pipelineBox),
		),
		termui.NewRow(1.0*.8,
			termui.NewCol(1.0, JobsBox),
		),
	)

	go func() {
		//TODO: Do something different w/ this attr. It comes from the 'demo' example in termui. It's just being used to do the flashing.
		count := 1
		highJob := 0
		for {
			select {
			case pipe := <-d.NewPipeChan:
				d.Pipeline = pipe
				commit := d.Server.GetCommitForProject(d.Project, d.Pipeline.Sha)
				d.Commit = &commit
				d.setupPipeWin(pipelineBox)
				d.setupJobsWin(JobsBox)
			case payload := <-d.ResizeChan:
				grid.SetRect(0, 0, payload.Width, payload.Height)
				termui.Clear()
				termui.Render(grid)
			case <-d.UpdateChan:

				updateParagraph(count)

				jobs := d.Server.GetJobsForPipeline(d.Project, d.Pipeline.ID)

				var jobStrings []string

				selectRow := -1
				for idx, job := range jobs {
					jobStrings = append(jobStrings, fmt.Sprintf(" %s (%s)", job.Name, job.Status))
					if job.Status == "running" {
						selectRow = idx
					}
				}

				if selectRow > 0 {
					JobsBox.SelectedRow = selectRow
					JobsBox.SelectedRowStyle.Fg = termui.ColorYellow
				} else {
					JobsBox.SelectedRowStyle.Fg = termui.ColorGreen
					JobsBox.SelectedRow = highJob
					highJob++
					if highJob >= len(jobs) {
						highJob = 0
					}
				}

				JobsBox.Rows = jobStrings
				width := JobsBox.Dx()
				JobsBox.SetRect(0, 5, width, len(jobStrings)+7)
				JobsBox.TextStyle.Fg = termui.ColorWhite

				termui.Clear()
				termui.Render(grid)
				count++
			}
		}
	}()
}

func (d *Display) setupPipeWin(paragraph *widgets.Paragraph) {
	paragraph.Title = fmt.Sprintf(" Pipeline - %d - %s", d.Pipeline.ID, fmt.Sprintf("%s ", d.Pipeline.Status))
	if d.Pipeline.Status == "success" {
		paragraph.TitleStyle.Fg = termui.ColorGreen
	} else if d.Pipeline.Status == "failed" {
		paragraph.TitleStyle.Fg = termui.ColorRed
	} else if d.Pipeline.Status == "running" {
		paragraph.TitleStyle.Fg = termui.ColorYellow
	} else {
		paragraph.TitleStyle.Fg = termui.ColorWhite
	}

	paragraph.Text = fmt.Sprintf(" %s -- %s\n Created_at: %s\n %s\n", d.Commit.Title, d.Pipeline.Sha[0:8], d.Pipeline.CreatedAt, d.Pipeline.WebURL)
	paragraph.TextStyle.Fg = termui.ColorWhite
	paragraph.BorderStyle.Fg = termui.ColorCyan
}

func (d *Display) setupJobsWin(list *widgets.List) {
	list.Title = " Jobs "
	list.Rows = []string{}
	list.TextStyle.Fg = termui.ColorYellow
}
