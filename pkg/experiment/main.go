package main

import (
	"fmt"
	"log"

	"github.com/itchyny/gojq"
)

func main() {
	query, err := gojq.Parse(".created_at | ..")
	if err != nil {
		log.Fatalln(err)
	}

	var input []interface{}
	//var input []map[string]interface{}
	var record = map[string]interface{}{
		"created_at": "2021-01-04T15:22:06.484Z",
		"id":         237130685,
		"ref":        "go-swagger",
		"sha":        "3dbb55c378ed1c61e255654331ab79121e6d2a13",
		"status":     "failed",
		"updated_at": "2021-01-04T15:23:22.778Z",
		"web_url":    "https://gitlab.com/theuberlab/thirteen/-/pipelines/237130685",
	}

	input = append(input, record)

	iter := query.Run(input) // or query.RunWithContext
	for {
		v, ok := iter.Next()
		if !ok {
			break
		}
		if err, ok := v.(error); ok {
			log.Fatalln(err)
		}
		fmt.Printf("%#v\n", v)
	}
}
