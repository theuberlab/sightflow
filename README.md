[![Pipeline Status](https://gitlab.com/theuberlab/sightflow/badges/master/pipeline.svg)](https://gitlab.com/theuberlab/sightflow/-/commits/master)
[![coverage](https://gitlab.com/theuberlab/sightflow/badges/master/coverage.svg)](https://ggitlab.com/theuberlab/sightflow/badges/master/coverage.svg)
[![GoDoc](https://godoc.org/gitlab.com/theuberlab/sightflow?status.svg)](https://godoc.org/gitlab.com/theuberlab/sightflow)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/theuberlab/sightflow)](https://goreportcard.com/report/gitlab.com/theuberlab/sightflowh)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![pkg.go.dev](https://pkg.go.dev/badge/gitlab.com/theuberlab/sightflow)](https://pkg.go.dev/gitlab.com/theuberlab/sightflow)

# SightFlow - The Gitlab Command Line Pipeline Watcher

A small tool which will allow you to easily view the status of your gitlab pipelines.

## About

This is a small command line utility to address a couple of issues watching Gitlab
pipelines. sightflow will get the status of the latest pipeline then
continually review and update the status of all jobs for that pipeline.

![sightflow](documentation/resources/sightflow.gif)

## Installation
1. Clone this repo
2. `go build -o sightflow ./cmd/main.go`
3. move sightflow into your path somewhere.

Altertatively download a pre-compiled binary for your favorite platform:

Linux: [ ![Download](https://api.bintray.com/packages/theuberlab/sightflow/sightflow_linux_amd64/images/download.svg) ](https://bintray.com/theuberlab/sightflow/sightflow_linux_amd64/_latestVersion)

MacOS: [ ![Download](https://api.bintray.com/packages/theuberlab/sightflow/sightflow_darwin_amd64/images/download.svg) ](https://bintray.com/theuberlab/sightflow/sightflow_darwin_amd64/_latestVersion)

Windows: [ ![Download](https://api.bintray.com/packages/theuberlab/sightflow/sightflow_windows_amd64/images/download.svg) ](https://bintray.com/theuberlab/sightflow/sightflow_windows_amd64/_latestVersion)

## Usage

At minimum sightflow requires a project ID to check.

`./sightflow -p 12345`

If the project in question is private sightflow requires a Gitlab access token with API permissions.
This should be set in the environment variable 'GITLAB_API_TOKEN'.

If your project is hosted on a provate instance of gitlab, sightflow requires the full
url including scheme (and port if non-standard.)

`./sightflow -p 12345 -u https://gitlab.theuberlab.com`

Run `sightflow -h` for usage.

```
$ sightflow -h
usage: Gitlab Pipeline Watcher --project-id=PROJECT-ID --gitlab-token=GITLAB-TOKEN [<flags>]

Watch the status of your pipelines.

Flags:
  -h, --help                   Show context-sensitive help (also try --help-long and --help-man).
      --version                Show application version.
      --log-utc                Timestamp lug.Log messages in UTC instead of local time. Can be set via sightflow_LOG_UTC
  -l, --logLevel=Error         The level of lug.Logging to use. Can be set via sightflow_LOG_LEVEL
  -f, --logformat=logfmt       What output format to use. Can be set via sightflow_LOG_FMT
  -p, --project-id=PROJECT-ID  Specify the numeric project ID to check the status of.
  -u, --gitlab-url="https://gitlab.com"  
                               Specify the url of the the gitlab instance to connect to. Can be set via the environment variable
                               GITLAB_URL.
  -t, --gitlab-token=GITLAB-TOKEN  
                               Specify the token to use to authenticate curl requests. NOTE: use the environment variable
                               GITLAB_API_TOKEN instead.
  -i, --interval=2             The refresh interval in seconds
```

## Known issues

There is very little checking of error conditions. Situations like specifying
a project ID that doesn't exist simply result in a pointer error.

May or may not work on Windows.

## Future

The ability to specify a list of jobs to report on instead of all.
The ability to specify a job for which sightflow will retun the logs for.
