package main

import (
	"fmt"
	"github.com/gizak/termui/v3"
	"github.com/prometheus/common/log"
	"gitlab.com/theuberlab/sightflow/pkg/gitlab"
	"gitlab.com/theuberlab/sightflow/pkg/ui"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"time"
)

// Command stores the settings and channels for a run of sightflow
type Command struct {
	projectID       int
	refreshInterval int
	gitlabServer    gitlab.GitlabServer
	//IncludeRegex	*regexp.Regexp
	//ExcludeRegex    *regexp.Regexp
	//includePattern 	string
	//excludePattern 	string
	lastPipeline 	gitlab.GLPipeLine
	refreshChan		chan bool
	newPipeChan		chan *gitlab.GLPipeLineFull
	resizeChan		chan termui.Resize
	display 		ui.Display
}

// NewCommand returns an instance of Command.
func NewCommand() Command {

	parsed, err := url.Parse(gitlabURL)
	if err != nil {
		log.Fatalf("Failed to parse provided URL: Error %s", err.Error())
	}

	port := 0
	if parsed.Port() != "" {
		port, _ = strconv.Atoi(parsed.Port())
	} else {
		switch parsed.Scheme {
		case "https":
			port = 443
		case "http":
			port = 80
		default:
			log.Fatalf("Unable to parse URL: Unknown scheme %s", parsed.Scheme)
		}
	}

	pipeline := gitlab.GLPipeLine{}
	commit := gitlab.GLCommit{}

	refreshChan := make(chan bool)
	newPipeChan := make(chan *gitlab.GLPipeLineFull)
	resizeChan := make(chan termui.Resize)

	gitlabServer :=   gitlab.GitlabServer{
		Scheme:   parsed.Scheme,
		Host:     parsed.Host,
		Port:     port,
		Token:    gitlabToken,
		BasePath: "/api/v4",
	}

	if includePattern != "" {
		gitlabServer.IncludeRegex, err = regexp.Compile(includePattern)
		if err != nil {
			log.Errorf("Unable to parse include regex: Error %s", err.Error())
			app.Usage(os.Args)
		}
	}

	if excludePattern != "" {
		gitlabServer.ExcludeRegex, err = regexp.Compile(excludePattern)
		if err != nil {
			log.Errorf("Unable to parse exclude regex: Error %s", err.Error())
			app.Usage(os.Args)
		}
	}

	return Command{
		projectID:       projectID,
		refreshInterval: refreshInterval,
		gitlabServer:	gitlabServer,
		lastPipeline:    pipeline,
		//includePattern: includePattern,
		//excludePattern: excludePattern,
		refreshChan:     refreshChan,
		resizeChan: 	resizeChan,
		newPipeChan:     newPipeChan,
		display: 		ui.Display{
			Project:     projectID,
			UpdateChan:  refreshChan,
			ResizeChan: resizeChan,
			NewPipeChan: newPipeChan,
			Server:      gitlabServer,
			Commit:      &commit,
		},
	}
}

// GetProjIDFromPath calls GitlabServer.GetProjectIDFromPath and prints the results to stdout.
func (c *Command) GetProjIDFromPath(path string) {
	projID := c.gitlabServer.GetProjectIDFromPath(path)
	fmt.Printf("Project ID: %d\n", projID)
	os.Exit(0)
}

// Run does what it says on the tin.
func (c *Command) Run() {
	c.display.Display()

	uiEvents := termui.PollEvents()
	pollTicker := time.NewTicker(time.Duration(pollInterval) * time.Second).C
	refreshTicker := time.NewTicker(time.Duration(refreshInterval) * time.Second).C

	c.lastPipeline = c.gitlabServer.GetLatestPipelineForProject(projectID)
	pipelineFull := c.gitlabServer.GetPipelineForProject(projectID, c.lastPipeline.ID)
	c.newPipeChan <- &pipelineFull
	c.refreshChan <- true

	// This is the primary loop for sightflow. It detects termui and ticker events and takes the appropriate action.
	for {
		select {
		case e := <-uiEvents:
			switch e.ID {
			case "q", "<C-c>":
				termui.Close()
				return
			case "<Resize>":
				payload := e.Payload.(termui.Resize)
				c.resizeChan <- payload

			//TODO: Implement detecting a mouse click. Pass the x and y (or just the whole payload) over to display, detect if it's within the pipeline box then call the external command open with the pipeline URL to open it in your browser.
			//case "<MouseLeft>":
			//	payload := e.Payload.(termui.Mouse)
			//	x, y := payload.X, payload.Y
			//
			}
		case <-pollTicker:
			checkPipe := c.gitlabServer.GetLatestPipelineForProject(projectID)
			if checkPipe != c.lastPipeline {
				c.lastPipeline = checkPipe
				newPipe := c.gitlabServer.GetPipelineForProject(projectID, c.lastPipeline.ID)
				c.newPipeChan <- &newPipe
			}

			c.refreshChan <- true
		case <-refreshTicker:
			c.refreshChan <- true
		}
	}
}
