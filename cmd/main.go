package main

import (
	"fmt"
	"github.com/prometheus/common/log"
	"gitlab.com/theuberlab/common-lang/go/version"
	"gitlab.com/theuberlab/lug"
	"gopkg.in/alecthomas/kingpin.v2"
	"os"
	"regexp"
	"strings"
)

// ####################################################################################################################
// TODOs
// TODO: Make this a plugin or two for TheWolf
// ####################################################################################################################

// General variables
var (
	logLevel        string
	logFormat       string
	logUTC          bool
	projectID       int
	gitlabURL       string
	gitlabToken     string
	refreshInterval int
	pollInterval	int
	appVersion      string
	cmdString       string
	includePattern 	string
	excludePattern 	string
	getProjectID	bool
)

// Variables that will be used as handles for specific activities.
var (
	// Creates the top level context for all commands flags and arguments
	app = kingpin.New("Gitlab Pipeline Watcher", "Watch the status of your pipelines.")
)

// Initialize some components that need to be available early on.
func init() {
	appVersion = version.GetVersionString()

	// Setup Kingpin flags
	// Set the application version number
	app.Version(appVersion)
	// Allow -h as well as --help
	app.HelpFlag.Short('h')

	// #########################################################################
	// Global flags

	// Logging flags
	app.Flag("log-utc", "Timestamp lug.Log messages in UTC instead of local time. Can be set via THIRTEEN_LOG_UTC").Default("false").Envar("THIRTEEN_LOG_UTC").BoolVar(&logUTC)
	app.Flag("logLevel", "The level of lug.Logging to use. Can be set via THIRTEEN_LOG_LEVEL").Short('l').Default("Error").Envar("THIRTEEN_LOG_LEVEL").EnumVar(&logLevel, "None",
		"Error",
		"Warn",
		"Info",
		"Debug",
		"All")
	app.Flag("logformat", "What output format to use. Can be set via THIRTEEN_LOG_FMT").Short('f').Default("logfmt").Envar("THIRTEEN_LOG_FMT").EnumVar(&logFormat, "logfmt", "json")

	app.Flag("project-id", "Specify the numeric project ID to check the status of.").Short('p').IntVar(&projectID)

	app.Flag("get-project-id", "Get the project ID for a project based on the current path. This assumes that the current path contains --gitlab-url. For example if $CWD is '/Users/aforster/go/src/gitlab.com/theuberlab/sightflow' and --gitlab-url contains 'https://gitlab.com' sightflow will fetch the numeri project ID for 'theuberlab/sightflow' on gitlab.com.").Short('g').BoolVar(&getProjectID)

	app.Flag("gitlab-url", "Specify the url of the the gitlab instance to connect to. Can be set via the environment variable GITLAB_URL.").Short('u').Envar("GITLAB_URL").Default("https://gitlab.com").StringVar(&gitlabURL)

	app.Flag("gitlab-token", "Specify the token to use to authenticate curl requests. NOTE: use the environment variable GITLAB_API_TOKEN instead.").Short('t').Envar("GITLAB_API_TOKEN").Required().StringVar(&gitlabToken)

	app.Flag("interval", "The interval, in seconds, at which to poll gitlab. NOTE: Not fully implemented!").Short('i').Default("5").IntVar(&pollInterval)

	app.Flag("refresh", "The interval, in seconds, at which to refresh the display").Short('r').Default("2").IntVar(&refreshInterval)

	app.Flag("include-pattern", "Only update for change requests against a REF which matches this regex.").Short('P').StringVar(&includePattern)

	app.Flag("exclude-pattern", "Do not update change requests against a REF which matches this regex.").Short('e').StringVar(&excludePattern)

	//-j, --job-name <job name>
	//	Specify the name (string) of the job to check the status of.
	//
	//-r, --retries
	//How many times to retry checking the status of the job.
	//
	//-s, --sleep
	//How long, in seconds, to wait in between each status check.

	// Now parse the flags
	cmdString = kingpin.MustParse(app.Parse(os.Args[1:]))

	if !getProjectID && projectID == 0 {
		kingpin.FatalUsage("Error: one of --project-id or --get-project-id must be provided.")
	}

	//TODO: Fix lug so that it works if I don't init from a file and make the file optional.
	lug.InitLoggerFromYaml("lugConfig.yml")
}

func main() {
	cmd := NewCommand()

	if getProjectID {
		cwd, err := os.Getwd()
		if err != nil {
			log.Fatalf("Error retrieving working directory: %s", err.Error())
		}
		glHostname := strings.TrimPrefix(gitlabURL, "https://")
		pathRegex := fmt.Sprintf("^[^\\s]+%s/([^\\s]+)$", glHostname)
		regex := regexp.MustCompile(pathRegex)

		matched := regex.Match([]byte(cwd))
		if !matched {
			kingpin.FatalUsage("Error parsing project from path: Is the base domain of --gitlab-url contained in your CWD?")
		}
		project := string(regex.FindSubmatch([]byte(cwd))[1])
		cmd.GetProjIDFromPath(project)
	} else {
		cmd.Run()
	}

}
