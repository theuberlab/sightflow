module gitlab.com/theuberlab/sightflow

go 1.15

require (
	github.com/alecthomas/units v0.0.0-20201120081800-1786d5ef83d4 // indirect
	github.com/gizak/termui/v3 v3.1.0
	github.com/itchyny/gojq v0.12.0
	github.com/magefile/mage v1.11.0
	github.com/prometheus/common v0.15.0
	github.com/sirupsen/logrus v1.7.0 // indirect
	gitlab.com/theuberlab/common-lang/go/version v0.0.4
	gitlab.com/theuberlab/lug v0.9.2
	golang.org/x/sys v0.0.0-20210110051926-789bb1bd4061 // indirect
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)
